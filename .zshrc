export TERM=xterm-vt220
# School
export PATH='${PATH}:/usr/school/bin/'
# Bocal
export PATH='${PATH}:/usr/site/sbin:/usr/site/bin'
# Heimdal
export PATH="${PATH}:/usr/heimdal/sbin:/usr/heimdal/bin"
# Eclipse
export PATH="${PATH}:/usr/bin/eclipse"
# Package
export PATH="${PATH}:/usr/local/sbin:/usr/local/bin"
# System
export PATH="${PATH}:/usr/sbin:/usr/bin:/sbin:/bin"

export HISTFILE="$HOME/.history"
export PAGER='less'
export EDITOR="emacs -nw"

# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="kphoen"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(rvm screen sublime debian dircycle encode64 gem git git-extras nyan lol github autojump command-not-found compleat svn ruby)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=/usr/sbin:/usr/bin:/sbin:/bin:/usr/school/sbin:/usr/school/bin:/usr/netsoul/sbin:/usr/netsoul/bin:/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/usr/bin/eclipse

alias gitlog='git log --graph --all --format=format:'\''%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n'\'''\''          %C(white)%s%C(reset) %C(bold white)— %an%C(reset)'\'' --abbrev-commit'
alias tube='display http://www.tfl.gov.uk/assets/downloads/standard-tube-map.gif'
alias j=jobs
alias emacs="emacs -nw"
alias ne=emacs
alias z='zlock -immed -pwtext "jKf5AztH is a fake password!"'
alias norme="$HOME/script/norme -score -libc -printline"
alias ducks='du -cks *|sort -rn|head -11'
alias dskload="ps faux|awk '\$8 ~ /D/{print}'"
alias do_make="$HOME/.myscripts/baba/do_make"
alias github="chrome \`git remote -v | grep github.com | grep fetch | head -1 | field 2 | sed 's/git:/http:/g'\`"
alias gdb="gdb --args"
alias t="tree"

wikipedia() {
    if [ "${1}" ]
    then
	dig +short "${1}".wp.dg.cx TXT
    fi
}
mkcd()
{
    if [ ${1} ]
    then
	mkdir -p ${1}
	cd ${1}
    else
	echo "mkcd: No arguments given !"
    fi
}

svnco()
{
    if [ ${1} ]
    then
	if [ ${2} ]
	then
	    svn checkout svn+ssh://kscm@koala-rendus.epitech.net/${1}-2017-${2}
	    cd ${1}-2017-${2}
	else
	    svn checkout svn+ssh://kscm@koala-rendus.epitech.net/${1}-2017-fillon_g
	    cd ${1}-2017-fillon_g
	fi
    else
	echo "mkcd: No arguments given !"
    fi
}

mk()
{
    make re
    make clean
    if [ ${1} ]
    then
	debug ./${@}
    fi
}

#--leak-check=full
debug()
{
    if [ ${1} ]
    then
	echo -n "" > debug.log
	valgrind --log-file=debug.log ${@}
    else
	echo "valgrind <PARAMS>"
    fi
}

clean()
{
    SEARCH='.'
    if [ ${1} ]
    then
	SEARCH=${1}
    fi
    find ${SEARCH} \( -name "*~" -or -name ".*~" \) -exec rm -fv {} \;
}

extract () {
    if [ -f $1 ] ; then
      case $1 in
        *.tar.bz2)   tar xjf $1     ;;
        *.tar.gz)    tar xzf $1     ;;
        *.bz2)       bunzip2 $1     ;;
        *.rar)       unrar e $1     ;;
        *.gz)        gunzip $1      ;;
        *.tar)       tar xf $1      ;;
        *.tbz2)      tar xjf $1     ;;
        *.tgz)       tar xzf $1     ;;
        *.zip)       unzip $1       ;;
        *.Z)         uncompress $1  ;;
        *.7z)        7z x $1        ;;
        *)     echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

export PERL_LOCAL_LIB_ROOT="/home/fillon_g/perl5";
export PERL_MB_OPT="--install_base /home/fillon_g/perl5";
export PERL_MM_OPT="INSTALL_BASE=/home/fillon_g/perl5";
export PERL5LIB="/home/fillon_g/perl5/lib/perl5/x86_64-linux-gnu-thread-multi:/home/fillon_g/perl5/lib/perl5";
export PATH="/home/fillon_g/perl5/bin:$PATH";
#EOF
export USER_NICKNAME="guillaume fillon"
